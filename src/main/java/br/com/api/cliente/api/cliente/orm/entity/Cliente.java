
package br.com.api.cliente.api.cliente.orm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "T_CLIENTE")
public class Cliente implements Serializable{
    
    private static final long serialVersionUID = 137616091650275296L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "nome")
    private String nome;
    
    @Column(name = "cpf")
    private String cpf;

    @Column(name = "dataNascimento")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "America/Sao_Paulo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataNascimento;
    
    public Integer getIdade(){
        
        LocalDate dataNasc = this.dataNascimento.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return Period.between(dataNasc, LocalDate.now()).getYears();
    }
}
