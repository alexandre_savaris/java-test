package br.com.api.cliente.api.cliente.business;

import br.com.api.cliente.api.cliente.orm.AbstractBusiness;
import br.com.api.cliente.api.cliente.orm.entity.Cliente;
import br.com.api.cliente.api.cliente.repository.ClienteRepository;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;


@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ClienteBusiness extends AbstractBusiness<Cliente> implements Serializable{
    
    @Autowired
    private ClienteRepository repository;
    
    @Override
    public ClienteRepository getRepository() {
        return repository;
    }
    
    public Page<Cliente> getPag(int page, int count){
        Pageable pages = PageRequest.of(page, count);
        return findAll(pages);
        
    }
}
