package br.com.api.cliente.api.cliente.dto;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteDTO {
    
    private Long id;
    private String nome;
    private String cpf;
    private Date dataNascimento;
}
