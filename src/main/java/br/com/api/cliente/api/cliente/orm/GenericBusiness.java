package br.com.api.cliente.api.cliente.orm;

import java.io.Serializable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GenericBusiness<T, ID extends Serializable> {

    public <S extends T> S save(S s);

    public T findById(ID id);

    public boolean existsById(ID id);

    public Iterable<T> findAll();

    public Page<T> findAll(Pageable pgbl);
    
    public long count();

    public void deleteById(ID id);

    public void delete(T t);
}

