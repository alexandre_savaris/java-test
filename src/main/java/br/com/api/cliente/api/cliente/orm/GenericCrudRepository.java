package br.com.api.cliente.api.cliente.orm;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface GenericCrudRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {

}

