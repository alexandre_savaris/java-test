package br.com.api.cliente.api.cliente.rest;

import br.com.api.cliente.api.cliente.business.ClienteBusiness;
import br.com.api.cliente.api.cliente.dto.ClienteDTO;
import br.com.api.cliente.api.cliente.dto.ResponseDTO;
import br.com.api.cliente.api.cliente.orm.entity.Cliente;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.assertj.core.util.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/clientes/")
public class ClienteRest {
    
    private final Logger LOGGER = LoggerFactory.getLogger(ClienteRest.class);
    
    @Autowired
    private ClienteBusiness clienteBusiness;
    
    @RequestMapping(method = RequestMethod.GET, consumes = {"application/json"}, produces = {"application/json"})
    public @ResponseBody ResponseEntity<Object> getClientes(){
        try{
            Iterable<Cliente> clientes = clienteBusiness.findAll();
            List<Cliente> listaClientes = Lists.newArrayList(clientes);
            return new ResponseEntity<Object>(listaClientes, HttpStatus.OK);            
        }catch(Exception e){
            LOGGER.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);            
        } 
    }
    
    @RequestMapping(method = RequestMethod.POST, consumes = {"application/json"}, produces = {"application/json"})
    public ResponseEntity salvarCliente(@RequestBody ClienteDTO clienteDto){
        try{
            Cliente cliente = new Cliente();
            cliente.setId(clienteDto.getId());
            cliente.setNome(clienteDto.getNome());
            cliente.setCpf(clienteDto.getCpf());
            cliente.setDataNascimento(clienteDto.getDataNascimento());
            clienteBusiness.save(cliente);
             return new ResponseEntity(new ResponseDTO(false, "OK", HttpStatus.CREATED), HttpStatus.CREATED);
        }catch(Exception e){
            LOGGER.error(e.getMessage());
            return new ResponseEntity(new ResponseDTO(true, e.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
        }   
    }
    
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> delete(@PathVariable(value = "id") Long id){
        try{
            clienteBusiness.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch(Exception e){
            LOGGER.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }   
    
    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Cliente> update(@RequestBody Cliente cli) {        
        return new ResponseEntity<>((Cliente) clienteBusiness.save(cli), HttpStatus.OK);
    }       
    
    @RequestMapping(value = "{page}/{count}", method = RequestMethod.GET, consumes = {"application/json"}, produces = {"application/json"})
    public Page<Cliente> findPage(HttpServletRequest request, @PathVariable int page, @PathVariable int count) {
         return clienteBusiness.getPag(page, count);
     }
    
}
