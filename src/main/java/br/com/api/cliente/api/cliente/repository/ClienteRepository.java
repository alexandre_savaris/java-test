package br.com.api.cliente.api.cliente.repository;

import br.com.api.cliente.api.cliente.orm.GenericCrudRepository;
import br.com.api.cliente.api.cliente.orm.entity.Cliente;

public interface ClienteRepository extends GenericCrudRepository<Cliente, Integer> {
    
       
}
