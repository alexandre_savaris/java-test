package br.com.api.cliente.api.cliente;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HikariDataSourceConfig {

    private static final String JDBC_URL_BASE = "jdbc:sqlserver://%s;databaseName=%s;integratedSecurity=false";

    @Bean(name = "dataSourceAPICliente")
    public DataSource getDataSource() throws SQLException {
        HikariConfig hc = new HikariConfig();
        hc.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

        String jdbcUrl = String.format(JDBC_URL_BASE, "177.10.162.19", "DadosAPI");

        hc.setJdbcUrl(jdbcUrl);
        hc.setPoolName("api-cliente-jpa-HikariCP");
        hc.setMaximumPoolSize(10);
        hc.setMinimumIdle(2);
        hc.setUsername("sa");
        hc.setPassword("@dell2019");
        HikariDataSource ds = new HikariDataSource(hc);

        return ds;
    }
}