package br.com.api.cliente.api.cliente.orm;

import java.io.Serializable;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public abstract class AbstractBusiness<T> implements GenericBusiness<T, Long>, Serializable {

    private static final long serialVersionUID = -7244441152481495952L;

    public abstract GenericCrudRepository getRepository();

    @Override
    public <S extends T> S save(S s) {
        return (S) getRepository().save(s);
    }

    @Override
    public T findById(Long id) {
        Optional<T> optional = getRepository().findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    @Override
    public boolean existsById(Long id) {
        return getRepository().existsById(id);
    }

    @Override
    public Iterable<T> findAll() {
        return getRepository().findAll();
    }

    @Override
    public long count() {
        return getRepository().count();
    }

    @Override
    public void deleteById(Long id) {
        getRepository().deleteById(id);
    }

    @Override
    public void delete(T t) {
        getRepository().delete(t);
    }
    
    @Override
    public Page<T> findAll(Pageable pgbl){
        return getRepository().findAll(pgbl);
    }
}
